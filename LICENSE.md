All binary software included unless otherwise specified are licensed under the
GPLv3 or later. Please reference `licenses/COPYING` for further information.

All library code included unless otherwise specified are licensed under the
LPGLv3 or later. Please reference `licenses/COPYING.LESSER` for further
information.

