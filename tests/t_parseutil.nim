import unittest

import epsilon/parseutil

suite "unit-tests":
  setup:
    const arglist = [
      "print",
      "sync",
      "download",
      "quit",
      "shutdown"
    ]

  test "Resolve":
    let cmd = arglist.resolve("pr")
    if cmd.isErr: fail()

    require(cmd.value == "print")
  
  test "Error":
    let cmd = arglist.resolve("t")
    require(cmd.isErr)
    require(cmd.error == NoMatches)

  test "Ambigous":
    let cmd = arglist.resolve("s")
    require(cmd.isErr)
    require(cmd.error == AmbiguousMatch)

