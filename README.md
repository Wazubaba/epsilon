![](logo/logo.gif)

## Introduction
Epsilon is a library that contains a ton of small snippets of code I've made
in one easy-to-use place. The purpose behind this is that rather than making
five thousand and one tiny libraries and never being able to remember what
is what it would be superior to simply bundle them. Not like I won't be using
them anyways after-all...

