# Package

version       = "0.1.0"
author        = "Wazubaba"
description   = "Collection of utilities and libraries to easy Wazubaba\'s development life"
license       = "hybrid"
installExt    = @["nim"]
bin           = @["epsilon"]

#backend       = "cpp"

# Dependencies

requires "nim >= 0.20.0"
requires "result >= 0.1.0"

