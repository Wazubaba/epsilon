## Implement compile-time definitions for standard paths with support for a
## command-line prefix to alter them if necessary.
##
## Please note that this will be refering to things as it would be called
## by on a linux system.

from os import join_path, PATHSEP


when defined(unix) or defined(bsd):
  const
    PREFIX* {.strdefine.}: string = "/"
    ETC* = join_path(PREFIX, "etc")
    VAR* = join_path(PREFIX, "var")
    HOME* = join_path(PREFIX, "home")
    OPT* = join_path(PREFIX, "opt")
    BIN* = join_path(PREFIX, "bin")
    LIB* = join_path(PREFIX, "lib")
    USR* = join_path(PREFIX, "usr")

# TODO: Figure this one out
when defined(windows):
  {.warning: "Currently the Windows implementation of syspaths uses Linux paths".}
  const
    PREFIX* {.strdefine.}: string = "C:\\"
    ETC* = join_path(PREFIX, "etc")
    VAR* = join_path(PREFIX, "var")
    HOME* = join_path(PREFIX, "home")
    OPT* = join_path(PREFIX, "opt")
    BIN* = join_path(PREFIX, "bin")
    LIB* = join_path(PREFIX, "lib")
    USR* = join_path(PREFIX, "usr")

# TODO: Figure this one out
when defined(osx):
  {.warning: "Currently the OSX implementation of syspaths uses Linux paths".}
  const
    PREFIX* {.strdefine.}: string = "/"
    ETC* = join_path(PREFIX, "etc")
    VAR* = join_path(PREFIX, "var")
    HOME* = join_path(PREFIX, "home")
    OPT* = join_path(PREFIX, "opt")
    BIN* = join_path(PREFIX, "bin")
    LIB* = join_path(PREFIX, "lib")
    USR* = join_path(PREFIX, "usr")

