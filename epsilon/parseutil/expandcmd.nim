import strutils
import result

export result.isErr, result.Result

type
  MatchFailure* = enum
    AmbiguousMatch,
    NoMatches

proc resolve*(argList: openArray[string], command: string): Result[string, MatchFailure] =
  ## Expand command to an element within argList, if possible.
  var matches: seq[string]

  for arg in argList:
    if command == arg:
      matches.add(arg)
      break
    
    if arg.startsWith(command):
      matches.add(arg)
  
  if matches.len > 1:
    result.err(AmbiguousMatch)
  elif matches.len == 0:
    result.err(NoMatches)
  else:
    result.ok(matches[0])
  return

